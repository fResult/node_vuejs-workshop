import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

import store from './store';

Vue.use(Router);
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/', name: 'home', component: Home, meta: {auth: true}
    },
    {
      path: '/equipments',
      name: 'equipment-list',
      component: () => import('./views/equipment/EquipmentList.vue'),
      meta: {auth: true}
    },
    {path: '/equipments/forms', name: 'equipment-form', component: () => import('./views/equipment/EquipmentForm.vue')},
    { // /////// Lazy Loading /////// //
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "register" */ './views/Register.vue'),
    },
    {path: '/login', name: 'login', component: () => import('./views/Login.vue')},
  ],
});

router.beforeEach((to, from, next) => {
  console.log(to.meta.auth);
  if (!to.meta.auth) next();
  // axios.get('api/account/user-session').then(() => next()).catch(() => next('login'));
  store.dispatch('get_user_session').then(() => next()).catch(() => next({name: 'login'}));
});

export default router;
