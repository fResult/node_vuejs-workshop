const customValidator = {
  custom: {
    u_username: {
      required: 'Username is required.',
      regex: `Username must be English characters and under score only. 
        Username must contains at least 6 characters`,
    },
    u_password: {
      required: 'Password is required.',
      regex: `Password cannot contains space
        Password must contains at least 8 characters.`,
    },
    u_firstName: {
      required: 'First Name is required.',
      regex: `First Name must be Thai or English characters only
        First Name cannot contains space`,
    },
    u_lastName: {
      required: 'Last Name is required.',
      regex: `Last Name must be Thai or English characters only
        Last Name cannot contains space`,
    },
  },
};

// const validator = Validator.localize('en', dict);

module.exports = customValidator;
