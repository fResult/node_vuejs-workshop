import Vue from 'vue';
import VueX from 'vuex';
import axios from 'axios';

Vue.use(VueX);

export default new VueX.Store({
  state: {
    user: null,
  },
  mutations: {
    set_user: (state, user) => {
      state.user = user;
    },
  },
  actions: {
    get_user_session: ({ commit }) => {
      axios.get('/api/account/user-session').then((res) => {
        commit('set_user', res.data);
      }).catch(ex => console.error(ex.message));
    },
  },
});
