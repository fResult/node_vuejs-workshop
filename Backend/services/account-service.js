const connection = require('../configs/database');
const {hash_password, verify_password} = require('../configs/security');

module.exports = {
  onRegister(payload) {
    return new Promise((resolve, reject) => {
      payload.u_password = hash_password(payload.u_password);
      connection.query('INSERT INTO tb_users SET ?', payload, (error, result) => {
        if (error) return reject(error);
        resolve(result);
      });
    });
  },

  onLogin: function (payload) {
    return new Promise((resolve, reject) => {
      connection.query('SELECT * FROM tb_users WHERE u_username = ?', [payload.u_username], (error, result) => {
        if (error) return reject(error);
        if (result.length > 0) {
          const loggedInUser = result[0];

          if (verify_password(payload.u_password, loggedInUser.u_password)) {
            delete loggedInUser.u_password;
            delete loggedInUser.u_created;
            delete loggedInUser.u_updated;

            resolve(loggedInUser);
          } else {
            reject(new Error('Username or Password is invalid.'))
          }
        } else {
          reject(new Error('Username or Password is invalid.'))
        }
      });
    })
  }
};
