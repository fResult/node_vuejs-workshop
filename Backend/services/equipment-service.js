const connection = require('../configs/database.js');

module.exports = {
  findAll(queryStr) {
    return new Promise((resolve, reject) => {
      const limitPage = 3;
      const startPage = ((queryStr.page || 1) - 1) * limitPage;
      const sqls = {
        equipRows: 'SELECT COUNT(*) AS length FROM tb_equipments',
        equips: 'SELECT * FROM tb_equipments'
      };

      if (queryStr.keySearch && queryStr.textSearch) {
        const key = queryStr.keySearch;
        const txt = queryStr.textSearch;
        const sqlSearchCond = ` WHERE ${connection.escapeId(key)} LIKE ${connection.escape(`%${txt}%`)}`;

        sqls.equipRows += sqlSearchCond;
        sqls.equips += sqlSearchCond;
      }

      if (queryStr.keySearch && queryStr.textSearch) {
        const key = queryStr.keySearch;
        const txt = queryStr.textSearch;
        const sqlSearchCond = ` WHERE ${connection.escapeId(key)} LIKE ${connection.escape(`%${txt}%`)}`;

        sqls.equipRows += sqlSearchCond;
        sqls.equips += sqlSearchCond;
      }

      // Count Equipment rows.
      connection.query(sqls.equipRows, (error, resultRows) => {
        if (error) reject(error);
        const equipments = {rows: resultRows[0].length, result: []};

        // Divide page of equipment list.
        sqls.equips += ` LIMIT ${connection.escape(startPage)}, ${limitPage}`;
        connection.query(sqls.equips, (error, resultEquips) => {
          if (error) reject(error);
          equipments.result = resultEquips;
          resolve(equipments);
        });
      });
    });
  },
  findOne(column) {
    return new Promise((resolve, reject) => {
      connection.query('SELECT * FROM tb_equipments WHERE ?', column, (error, result) => {
        if (error) reject(error);
        resolve(result.length > 0 ? result[0] : null);
      })
    })
  },
  onCreate(equipPayload) {
    let sqlInsertEquip = `INSERT INTO tb_equipments SET ?`;
    return new Promise((resolve, reject) => {
      connection.query(sqlInsertEquip, equipPayload, (error, result) => {
        if (error) reject(error);
        resolve(result);
      });
    });
  },
  onDelete(id) {
    return new Promise((resolve, reject) => {
      connection.query('DELETE FROM tb_equipments WHERE eq_id=?', [id], (error, result) => {
        if (error) reject(error);
        resolve(result);
      })
    });
  },
  onUpdate(id, equipInfo) {
    return new Promise((resolve, reject) => {
      const $sql = `
        UPDATE tb_equipments SET
            eq_name = ?,
            eq_detail = ?,
            eq_image = ?
        WHERE
            eq_id = ?
      `;
      connection.query($sql, [equipInfo.eq_name, equipInfo.eq_detail, equipInfo.eq_image, id],
          (error, result) => {
            if (error) reject(error);
            resolve(result);
          });
    });
  }
};
