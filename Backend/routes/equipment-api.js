const router = require('express').Router();
const equipService = require('../services/equipment-service.js');
const {check, query} = require('express-validator');
const base64Img = require('base64-img');
const fs = require('fs');
const path = require('path');
const uploadDir = path.resolve('uploads');
const equipDir = path.join(uploadDir, 'equipments');

// Display Equipment info
router.get('/', [
  query('page').not().isEmpty().isInt().toInt()
], async (req, res) => {
  try {
    req.validate();

    res.json(await equipService.findAll(req.query));
  } catch (ex) {
    res.error(ex);
  }
});

router.post('/', [
  check('eq_name').not().isEmpty(),
  check('eq_image').not().isEmpty(),
], async (req, res) => {
  try {
    req.validate();

    // Check Directory, and folder will be created if not exist.
    if (!fs.existsSync(uploadDir))
      fs.mkdirSync(uploadDir);
    if (!fs.existsSync(equipDir))
      fs.mkdirSync(equipDir);

    // Convert Base64 to Image and save into "uploads/equipments" directory.
    req.body.eq_image = base64Img
        .imgSync(req.body.eq_image, equipDir, `Equip-${Date.now()}`)
        .replace(`${equipDir}\\`, '');
    res.json({message: await equipService.onCreate(req.body)});
  } catch (ex) {
    // Delete Image file when Insert data into tb_equipments doesn't be passed.
    const imageFile = path.join(equipDir, req.body.eq_image);
    if (fs.existsSync(imageFile)) fs.unlinkSync(imageFile);

    res.error(ex);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    // Find Equipment info before delete
    const equipment = await equipService.findOne({eq_id: req.params.id});
    if (!equipment) throw new Error('Item not found.');

    const equipDelete = await equipService.onDelete(equipment.eq_id);

    const imageFile = path.join(equipDir, equipment.eq_image);
    if (fs.existsSync(imageFile)) fs.unlinkSync(imageFile);

    res.send(equipDelete);
  } catch (ex) {
    res.error(ex);
  }
});

// Edit Equipment info
router.put('/:id', [
  check('eq_name').not().isEmpty(),
  check('eq_image').not().isEmpty(),
], async (req, res) => {
  try {
    req.validate();

    // Find Equipment info before delete
    const equipment = await equipService.findOne({eq_id: req.params.id});
    if (!equipment) throw new Error('Item not found.');

    // Check Directory, and folder will be created if not exist.
    if (!fs.existsSync(uploadDir))
      fs.mkdirSync(uploadDir);
    if (!fs.existsSync(equipDir))
      fs.mkdirSync(equipDir);

    // Convert Base64 to Image and save into "uploads/equipments" directory.
    req.body.eq_image = base64Img
        .imgSync(req.body.eq_image, equipDir, `Equip-${Date.now()}`)
        .replace(`${equipDir}\\`, '');

    const equipUpdate = await equipService.onUpdate(req.params.id, req.body)

    // Delete old picture when Update info success.
    if (equipUpdate.affectedRows > 0) {
      const imageFile = path.join(equipDir, equipment.eq_image);
      if (fs.existsSync(imageFile)) fs.unlinkSync(imageFile);
    }

    res.json(equipUpdate);
  } catch (ex) {
    res.error(ex);
  }
});

module.exports = router;
