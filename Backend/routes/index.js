const router = require('express').Router();
const {authenticated} = require('../configs/security.js');

// root api for account
router.use('/account', require('./account-api'));

// root api for equipment
router.use('/equipment', authenticated, require('./equipment-api'));

module.exports = router;
