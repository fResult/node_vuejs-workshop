const router = require('express').Router();
const {check, validationResult} = require('express-validator');
const {onRegister, onLogin} = require('../services/account-service');

router.post('/register', [
  check('u_username').not().isEmpty(),
  check('u_password').not().isEmpty(),
  check('u_firstName').not().isEmpty(),
  check('u_lastName').not().isEmpty(),
], async (req, res) => {
  try {
    req.validate();
    res.json(await onRegister(req.body));
  } catch (ex) {
    res.error(ex);
  }
});

router.post('/login', [
  check('u_username').not().isEmpty(),
  check('u_password').not().isEmpty(),
], async (req, res) => {
  try {
    req.validate();
    const loggedInUser = await onLogin(req.body);

    // Assign session
    req.session.loggedInUser = loggedInUser;
    res.json(loggedInUser);
  } catch (ex) {
    res.error(ex);
  }
});

// Check logged in user
router.get('/user-session', (req, res) => {
  try {
    if (req.session.loggedInUser) {
      res.json(req.session.loggedInUser);
    } else {
      throw new Error('Unauthorized access.')
    }
  } catch (ex) {
    res.error(ex, 401);
  }
});

router.get('/logout', (req, res) => {
  try {
    delete req.session.loggedInUser;
    res.json({message: 'Logged out.'});
  } catch (ex) {
    res.error(ex);
  }
});

module.exports = router;
