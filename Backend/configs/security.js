const crypto = require('crypto');

const security = {
  hash_password(password) {
    return crypto.createHash('sha1').update(password).digest('hex');
  },
  verify_password(password, password_hash) {
    return security.hash_password(password) === password_hash;
  },
  authenticated(req, res, next) {
    req.session.loggedInUser = {
      u_firstName: 'Sila',
      u_lastName: 'Setthakan-anan',
      u_username: 'Korn704',
      u_password: '12345678'
    };
    try {
      if (req.session.loggedInUser) {
        return next();
      }
      throw new Error('Unauthorized.');
    } catch (ex) {
      res.error(ex, 401);
    }
  }
};

module.exports = security;
