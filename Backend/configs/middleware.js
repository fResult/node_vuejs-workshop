const {validationResult} = require('express-validator');

module.exports = function (req, res, next) {

  // validate json value not null
  req.validate = function () {
    const errors = validationResult(req).array();
    if (errors.length === 0) return;
    // throw new Error(`${errors[0].param} : ${errors[0].msg}`);

    let errorMessage = ``;
    errors.forEach((error, idx) => {
      if (idx !== errors.length - 1) {
        errorMessage += `${error.param} : ${error.msg} | `
      } else {
        errorMessage += `${error.param} : ${error.msg}`
      }
    });

    throw new Error(`${errorMessage}`);
  };

  // throw error out
  res.error = function (ex, status = 400) {
    res.status(status).json({errorMessage: ex.message});
  };
  next();
};
