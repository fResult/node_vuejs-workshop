const express = require('express');
const server = express();
const expressSession = require('express-session');
const bodyParser = require('body-parser');
const PORT = 3000;

// Set Session for System
server.use(expressSession({
  secret: '%T$h&e*P@ssw0rd)',
  resave: false,
  saveUninitialized: true,
  cookie: {}
}));


// Set Variable Parser when client request
server.use(bodyParser.urlencoded({extended: false}));
server.use(bodyParser.json({limit: '500MB'})); // Return JSON to client

// Create Custom function
server.use(require('./configs/middleware'));

// root api (call routes)
server.use('/api', require('./routes'));

server.get('*', (req, res) => {
  res.end(`<h1>Backend server is started</h1>`) //test show session
});

server.listen(PORT, () => console.log(`Server is started. PORT ${PORT}`))
